Docker lamp builder Change Log
==============================

0.0.2 August 7, 2019
------------------------

- Updated deploy-env.sh and template file to generate docker-compose 
- Updated Dockerfile.template files for mariadb, mysql, php-apache
- Added init.sh script to iterate through script files in init.d
- Updated deploy-env.sh to take argument parameters


0.0.1 July 26, 2019
--------------------

- Initial release.