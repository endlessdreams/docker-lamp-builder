#!/bin/bash


#yell() { printf "%s" "$0: $*" >&2; }
##die() { yell "$*"; exit 111; }
#die() { yell "$*"; brreak; }
#try() { "$@" || die "cannot $*"; }

current_script=$(basename "$0")

usage()
{
    echo "usage: ${current_script} [[[-b|--RUN-IN-BUILD-PHASE true|false] [-s|--POST-BUILD-SCRIPT postBuildScript ] [-f|--POST-BUILD-FROM-STEP postBuildFromStep] [-t|--POST-BUILD-TO-STEP postBuildToStep] [--SET-WORKING-DIR working_dir] [--SET-GIT-REPO git_repo] [--SET-GIT-REPO-version git_repo_version]] | [-h]]"
}

working_dir=$(pwd)
postBuildFromStep=4
postBuildToStep=9
postBuildScript=
isInBuildPhase=false

while :; do
    case $1 in
        -b|--RUN-IN-BUILD-PHASE)    isInBuildPhase="true"
                                    ;;
        -s|--POST-BUILD-SCRIPT)     shift
                                    postBuildScript=$1
                                    printf "%s\n" "postBuild"
                                    ;;
        -f|--POST-BUILD-FROM-STEP)  shift
                                    postBuildFromStep=$1
                                    ;;
        -t|--POST-BUILD-TO-STEP)    shift
                                    postBuildToStep=$1
                                    ;;
        --SET-WORKING-DIR)          shift
                                    working_dir=$1
                                    ;;
        --SET-GIT-REPO)             shift
                                    git_repo=$1
                                    ;;
        --SET-GIT-REPO-VERSION)     shift
                                    git_repo_version=$1
                                    ;;
        *) break
    esac
    shift
done


if [[ $postBuildFromStep -lt 1 ]]  ; then
  preBuildToStep=
else
  preBuildToStep="$(($postBuildFromStep-1))"
  if [[ preBuildToStep -eq 0 ]]  ; then
    preBuildToStepFirstDigit="${preBuildToStep}"
  else
    preBuildToStepFirstDigit="$0-${preBuildToStep}"
  fi
fi



if [[ $postBuildFromStep -eq $postBuildToStep ]]  ; then
  postBuildToStepFirstDigit="${postBuildFromStep}"
else
  postBuildToStepFirstDigit="${postBuildFromStep}-${postBuildToStep}"
fi

printf "%s\n" "isInBuildPhase: $isInBuildPhase"

if [ "${isInBuildPhase}" != "true" ] ; then

  # Post build run

  printf "%s\n" "Post build run: ENV_RUN_INIT_SCRIPTS_IN_BUILD=$ENV_RUN_INIT_SCRIPTS_IN_BUILD,  ^[0-${preBuildToStep}][0-9][\-] ^[${postBuildToStepFirstDigit}][0-9][\-]"

  if [ "${ENV_RUN_INIT_SCRIPTS_IN_BUILD}" != "true" ] ; then
    printf "%s\n" "Run when not running init scripts in build"
    for filepath in /var/build/init.d/*.sh; do
#        [ -e "$filepath" ] || continue
        [[ "${preBuildToStep}" = "" ]] || continue
        # ... rest of the loop body
        # shellcheck disable=SC2076
        if [[ ${filepath##*/} =~ "^[0-${preBuildToStep}][0-9][\-]" ]] ; then
            printf "%s\n" "Running(post build): ${filepath##*/}"
            exec "${filepath}" --SET-GIT-REPO "${git_repo}" --SET-GIT-REPO-VERSION "${git_repo_version}" --SET-WORKING_DIR "${working_dir}"
        fi
    done
  fi

printf "%s\n" "Before Running(post build)"
  for filepath in /var/build/init.d/*.sh; do
    printf "%s\n" "Running(post build): ${filepath##*/}"
#      [ -e "$filepath" ] || continue
      # ... rest of the loop body
      # shellcheck disable=SC2076
      if [[ ${filepath##*/} =~ ^[${postBuildToStepFirstDigit}][0-9][\-] ]] ; then
#      if [[ ${filepath##*/} =~ ^[4][0-9][\-] ]] ; then
          printf "%s\n" "Running(post build): ${filepath##*/}"
#          try cd "${oldpwd}"
          exec "${filepath}" --SET-GIT-REPO "${git_repo}" --SET-GIT-REPO-VERSION "${git_repo_version}" --SET-WORKING_DIR "${working_dir}"
      fi
  done




else
    # Build run

  if [ "${ENV_RUN_INIT_SCRIPTS_IN_BUILD}" = "true" ] ; then

    if [ "${postBuildScript}" != "" ] ; then
      toutch /var/www/testfile.txt
#      printf "%s\n" "10"
#      rm -rf /var/www/*
#      printf "%s\n" "20"
#      rm -rf /var/www/.ssh
#      printf "%s\n" "30"
#      cp /var/build/.gitconfig /var/www/.gitconfig
#      printf "%s\n" "40"
#      cp -ar /root/www/.ssh /var/www/.ssh
#      printf "%s\n" "50"
#      mv -ar /var/build/cassis /var/www/cassis
#      printf "%s\n" "60"
      exec "${postBuildScript}"
    else

      for filepath in /var/build/init.d/*.sh; do
#          [[ ! ${filepath##*/} =~ ^[0] ]] || continue
          [[ "${preBuildToStep}" = "" ]] || continue
          # ... rest of the loop body
          # shellcheck disable=SC2076
#          if [[ ${filepath##*/} =~ "^[0-preBuildToStep][0-9][\-]" ]] ; then
          if [[ ${filepath##*/} =~ "^[0-3][0-9][\-]" ]] ; then
              printf "%s\n" "Running(build): ${filepath##*/}"
              printf "%s\n" "working_dir: ${working_dir}"
              exec "${filepath}" --SET-GIT-REPO "${git_repo}" --SET-GIT-REPO-VERSION "${git_repo_version}" --SET-WORKING_DIR "${working_dir}"
          fi
      done
    fi

  fi

fi