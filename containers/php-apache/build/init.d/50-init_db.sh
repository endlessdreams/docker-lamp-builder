#!/bin/bash
# Run database initatiation

working_dir=$(pwd)
git_repo=git@bitbucket.org:endlessdreams/cassis.git
git_repo_version=0.9

while :; do
    case $1 in
        --SET-WORKING-DIR)          shift
                                    printf "%s\n" "working_dir: ${working_dir}"
                                    working_dir=$1
                                    printf "%s\n" "working_dir: ${working_dir}"
                                    ;;
        --SET-GIT-REPO)             shift
                                    git_repo=$1
                                    ;;
        --SET-GIT-REPO-VERSION)     shift
                                    git_repo_version=$1
                                    ;;
        *) break
    esac
    shift
done

if [ -d "${working_dir}/${APPLICATION_NAME}" ]; then
  printf "%s\n" "Start database initiation and population of application"
  cd "${working_dir}/${APPLICATION_NAME}" || printf "%s\n" "Failed to change directory" && exit 1

  # php yii databse/init --interactive=0
  printf "%s\n" "Finished database initiation and population of application"
else
  printf "%s\n" "Failed to run database initiation of application"
  exit 1
fi
