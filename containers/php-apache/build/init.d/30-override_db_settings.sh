#!/bin/bash
# Set up yii db properties for current db server enviroment
working_dir=$(pwd)
git_repo=git@bitbucket.org:endlessdreams/cassis.git
git_repo_version=0.9

while :; do
    case $1 in
        --SET-WORKING-DIR)          shift
                                    printf "%s\n" "working_dir: ${working_dir}"
                                    working_dir=$1
                                    printf "%s\n" "working_dir: ${working_dir}"
                                    ;;
        --SET-GIT-REPO)             shift
                                    git_repo=$1
                                    ;;
        --SET-GIT-REPO-VERSION)     shift
                                    git_repo_version=$1
                                    ;;
        *) break
    esac
    shift
done

if [ "${working_dir}" = "/var/www" ]; then
  set -e
fi

if [ -d "${working_dir}/${APPLICATION_NAME}" ]; then
  printf "%s\n" "Override web files in application"
  cd "${working_dir}/${APPLICATION_NAME}"

  printf "%s\n" "$(ls /var/build/php/dev/*)"
  cp -ar /var/build/php/dev/* .

  FINISHED_BUILD_INIT=`basename "$0"`
  export FINISHED_BUILD_INIT
  printf "%s\n" "Finished override web files in application"
else
  printf "%s\n" "Failed to override web files in application"
  exit 3
fi
