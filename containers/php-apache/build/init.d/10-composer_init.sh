#!/bin/bash
# Build application
working_dir=$(pwd)
git_repo_default=git@bitbucket.org:endlessdreams/cassis.git
git_repo_version_default=0.9

while :; do
    case $1 in
        --SET-WORKING-DIR)          shift
                                    printf "%s\n" "working_dir: ${working_dir}"
                                    working_dir=$1
                                    printf "%s\n" "working_dir: ${working_dir}"
                                    ;;
        --SET-GIT-REPO)             shift
                                    git_repo=$1
                                    ;;
        --SET-GIT-REPO-VERSION)     shift
                                    git_repo_version=$1
                                    ;;
        *) break
    esac
    shift
done

working_dir=$(realpath "$working_dir:-.")

printf "%s\n" "Remove old application ${APPLICATION_NAME} and copy credentials"
if [ "${git_repo}" = "" ]; then
  git_repo=git_repo_default
fi

if [ "${git_repo_version}" = "" ]; then
  git_repo_version=git_repo_version_default
fi


if [ "${working_dir}" = "/var/www" ]; then
  set -e
fi
#  rm -rf /var/www/*

if [ -d "${working_dir}/${APPLICATION_NAME}" ]; then
  rm -rf "${working_dir}/${APPLICATION_NAME}"
fi

if [ -d "${working_dir}/.ssh" ]; then
  printf "%s\n" "-10"
#  ls -la /var/www/
  rm -rf "${working_dir}/.ssh"
fi

#ls -la /var/www/
#ls -la /root/www/

cp -ar /var/build/www/.ssh "${working_dir}/.ssh"

if [ -d "${working_dir}/.gitconfig" ]; then
  rm -rf "${working_dir}/.gitconfig"
fi
cp /var/build/www/.gitconfig "${working_dir}/.gitconfig"



if [ -d "${working_dir}" ]; then
  printf "%s\n" "Set working directory to ${working_dir}"
  printf "%s\n" "Set working directory to web root"
  cd "${working_dir}"
  printf "%s\n" "Start clone application ${APPLICATION_NAME}"
  git clone --single-branch --branch "${git_repo_version}" "${git_repo}" "${APPLICATION_NAME}"
  printf "%s\n" "Finished clone application"


  if [ -d "${working_dir}/${APPLICATION_NAME}" ]; then
    printf "%s\n" "Start to composer init application for ${APPLICATION_NAME}"
    cd "${working_dir}/${APPLICATION_NAME}"

    rm -rf composer.lock
    rm -rf vendor/*


    #php -d memory_limit=-1 /usr/local/bin/composer.phar install --prefer-source
    php -d memory_limit=-1 /usr/local/bin/composer.phar install

    FINISHED_BUILD_INIT=`basename "$0"`
    export FINISHED_BUILD_INIT
    printf "%s\n" "Finished to composer init application"
  #else
  #  printf "%s\n" "Failed to composer init application"
  #  exit 1
  fi


#else
#  printf "%s\n" "Failed to clone application"
#  exit 1
fi


