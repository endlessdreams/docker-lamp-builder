#!/bin/bash
# Run yii init
working_dir=$(pwd)
git_repo=git@bitbucket.org:endlessdreams/cassis.git
git_repo_version=0.9

while :; do
    case $1 in
        --SET-WORKING-DIR)          shift
                                    printf "%s\n" "working_dir: ${working_dir}"
                                    working_dir=$1
                                    printf "%s\n" "working_dir: ${working_dir}"
                                    ;;
        --SET-GIT-REPO)             shift
                                    git_repo=$1
                                    ;;
        --SET-GIT-REPO-VERSION)     shift
                                    git_repo_version=$1
                                    ;;
        *) break
    esac
    shift
done

working_dir=$(realpath "$working_dir")

printf "%s\n" "Init application"
if [ "${working_dir}" = "/var/www" ]; then
  set -e
fi

if [ -d "${working_dir}/${APPLICATION_NAME}" ]; then
  cd "${working_dir}/${APPLICATION_NAME}"


  php init --env=Common --overwrite=All
  php init --env=Development --overwrite=All

  FINISHED_BUILD_INIT=`basename "$0"`
  export FINISHED_BUILD_INIT
  printf "%s\n" "Finished Init application"
else
  printf "%s\n" "Failed to init application"
  exit 2
fi
