#!/bin/bash
# Post build
working_dir=$(pwd)

set -e

printf "%s\n" "postBuild"

exec "$@"
