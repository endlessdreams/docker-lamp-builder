<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=db;dbname='.getenv('MYSQL_DATABASE'),
            'username' => getenv('MYSQL_USER'),
            'password' => getenv('MYSQL_PASSWORD'),
            'charset' => 'utf8',
        ],
        'db2' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=db2;dbname='.getenv('POSTGRES_DATABASE'),
            'tablePrefix' => null,
            'username' => getenv('POSTGRES_USER'),
            'password' => getenv('POSTGRES_PASSWORD'),
            'charset' => 'utf8',
        ],
        'db3' => [
            'class' => 'yii\db\Connection',
            'driverName' => 'mariadb',
            'schemaMap' => [
                'mariadb' => SamIT\Yii2\MariaDb\Schema::class
            ],
            'dsn' => 'mariadb:host=db;dbname='.getenv('MARIADB_DATABASE'),
            'username' => getenv('MARIADB_USER'),
            'password' => getenv('MARIADB_PASSWORD'),
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];

//            'dsn' => 'mysql:host=db;dbname='.getenv('MYSQL_DATABASE'),
//            'driverName' => 'mariadb',
//            'schemaMap' => [
////                'mysql' => SamIT\Yii2\MariaDb\Schema::class
////            ],