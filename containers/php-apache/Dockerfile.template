FROM yiisoftware/yii2-php:7.3-apache
LABEL maintainer="kjellake.lundblad@endlessdreams.biz"

SHELL ["/bin/bash", "-c"]

ARG RUN_INIT_SCRIPTS_IN_BUILD=true
ARG WORKING_DIR=/var/www
ARG POST_BUILD_FROM_STEP=4
ARG GIT_REPO=git@bitbucket.org:endlessdreams/cassis.git
ARG GIT_REPO_VERSION=0.9
ENV APPLICATION_NAME=cassis
ENV ENV_RUN_INIT_SCRIPTS_IN_BUILD=$RUN_INIT_SCRIPTS_IN_BUILD

#install some basic tools
RUN apt-get clean && \
    apt-get update && \
    apt-get install -y \
        tree \
        vim \
        wget \
        curl \
        jq \
        sudo

#install some base extensions
RUN apt-get install -y libxslt-dev \
    && docker-php-ext-install -j$(nproc) calendar gettext xsl
#RUN docker-php-ext-install pdo_pgsql pgsql ldap opcache

RUN chmod a+rx /usr/local/bin/composer


# add credentials to www on build
RUN mkdir -p /var/build/www/.ssh/ && \
    chmod 0700 /var/build/www/.ssh

COPY /${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_RELPATH}/php-apache/www/.ssh/id_rsa /containers/php-apache/www/.ssh/id_rsa  /var/build/www/.ssh/
COPY /${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_RELPATH}/php-apache/www/.ssh/id_rsa.pub /containers/php-apache/www/.ssh/id_rsa.pub /var/build/www/.ssh/
RUN chmod 600 /var/build/www/.ssh/id_rsa && \
    chmod 600 /var/build/www/.ssh/id_rsa.pub

# make sure your domain is accepted
RUN touch /var/build/www/.ssh/known_hosts && \
    ssh-keyscan bitbucket.org >> /var/build/www/.ssh/known_hosts

# Set git environment
COPY /${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_RELPATH}/php-apache/www/gitconfig.txt /var/build/www/.gitconfig

# Copy HTTP server config
ADD /${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_RELPATH}/php-apache/000-default.conf /etc/apache2/sites-available/

# Get build scripts and override
COPY /${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_RELPATH}/php-apache/build /var/build/
ADD /${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_RELPATH}/php-apache/build/init.d  /containers/php-apache/build/init.d /var/build/init.d/
RUN chmod 0755 /var/build && \
    chmod -R 0750 /var/build/init.d && \
    chmod 0755 /var/build/init.sh && \
    chown -R www-data:www-data /var/build && \
    chown www-data:www-data /var/www && \
    chown -R www-data:www-data /var/build/www/.ssh && \
    chown www-data:www-data /var/build/www/.gitconfig

WORKDIR /var/www/
USER www-data

# WORKDIR "${WORKING_DIR}"
# USER root
# TODO: Set up a volume as the edited module folder as source and composer vendor package root folder as target. Try to do this with env vars.

# TODO: Add modes Develop and Production: Set Prodoction as default. Enable xdebug in debug mode.
# TODO: Find out a way to set global (from host point of view) set host ip/dns name by using env var.
# Add this on host:
  #<VirtualHost *:80>
  #  ProxyPreserveHost On
  #  ProxyRequests Off
  #  ServerName docker-${APPLICATION_NAME}.local
  #  ServerAlias www.docker-${APPLICATION_NAME}.local
  #  ProxyPass http://docker-${APPLICATION_NAME}.local http://docker-${APPLICATION_NAME}.local:8080
  #  ProxyPassReverse http://docker-${APPLICATION_NAME}.local http://docker-${APPLICATION_NAME}.local:8080
  #</VirtualHost>

RUN bash /var/build/init.sh --RUN-IN-BUILD-PHASE ${ENV_RUN_INIT_SCRIPTS_IN_BUILD} \
    --SET-GIT-REPO "${GIT_REPO}" --SET-GIT-REPO-VERSION "${GIT_REPO_VERSION}" \
    --SET-WORKING_DIR "${WORKING_DIR}" --POST-BUILD-FROM-STEP "${POST_BUILD_FROM_STEP}"

USER root