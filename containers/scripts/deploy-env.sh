#!/usr/bin/env bash

# Setting up variables

current_script=$(basename "$0")
DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH=$(dirname "$(dirname "$(readlink -f "$0")")")
export DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH
DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER=$(basename "${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}")
export DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER


function relative_path_from_to() {
  # strip trailing slashes
  path1=${1%\/}
  path2=${2%\/}
  # common part of both paths
  common=$(printf '%s\x0%s' "${path1}" "${path2}" | sed 's/\(.*\).*\x0\1.*/\1/')
  # how many directories we have to go up to the common part
  up=$(grep -o "/" <<< "${path1#$common}" | wc -l)
  # create a prefix in the form of ../../ ...
  prefix=""; for ((i=0; i<=$up; i++)); do prefix="$prefix../"; done
  pre=${2#$common}
  printf "%s" "${pre#/}"
}



usage()
{
    echo "usage: ${current_script} [[[-p projectrootpath] [-w webrootpath ] [-b apachebuildscriptpath] [-a applicationname] [--build-with-init-scripts true|false] [-i]] | [-h]]"
}

application_name=cassis
do_run_init_scripts_in_build=true

projectrootpath=$(dirname "${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}")

#interactive=
webrootpath=${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/web
apachebuildscriptpath=${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/build/init.d

##### Main
while [ "$1" != "" ]; do
    case $1 in
        -p | --project-root )       shift
                                    projectrootpath=$(realpath "$1")
                                    ;;
        -w | --web-root )           shift
                                    webrootpath=$1
                                    ;;
        -b | --apachescript )       shift
                                    apachebuildscriptpath=$1
                                    ;;
        --build-with-init-scripts ) shift
                                    do_run_init_scripts_in_build=$1
                                    ;;
        -a | --application-name )   shift
                                    application_name=$1
                                    ;;
#        -i | --interactive )    interactive=1
#                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

export APPLICATION_NAME=${application_name}
export RUN_INIT_SCRIPTS_IN_BUILD=${do_run_init_scripts_in_build}

webrootpath_relpath=$(relative_path_from_to ${projectrootpath} ${webrootpath})
apachebuildscriptpath_relpath=$(relative_path_from_to ${projectrootpath} ${apachebuildscriptpath})

export DOCKER_COMPOSE_WEB_ROOT=${webrootpath_relpath}
export PHP_APACHE_SCRIPT_BUILD_PATH=${apachebuildscriptpath_relpath}

DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_RELPATH=$(relative_path_from_to ${projectrootpath} ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH})
export DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_RELPATH


export DOLLAR='$'

# Create files

rm -rf "${projectrootpath}/docker-compose.yml";
envsubst < "${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/scripts/template.yml" > "${projectrootpath}/docker-compose.yml";


rm -rf ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/root/gitconfig.txt &&
envsubst < ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/www/gitconfig.template > ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/www/gitconfig.txt;

rm -rf ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/mariadb/Dockerfile &&
envsubst < ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/mariadb/Dockerfile.template > ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/mariadb/Dockerfile;

rm -rf ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/mysql/Dockerfile &&
envsubst < ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/mysql/Dockerfile.template > ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/mysql/Dockerfile;

rm -rf ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/Dockerfile &&
envsubst < ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/Dockerfile.template > ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/Dockerfile;

rm -rf ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/000-default.conf &&
envsubst < ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/000-default.conf.template > ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/000-default.conf;

rm -rf ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/postgres/Dockerfile &&
envsubst < ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/postgres/Dockerfile.template > ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/postgres/Dockerfile;


cp ${MY_CASSIS_BITBUCKET_USER_SECRET_KEY_PATH} ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/www/.ssh/id_rsa
cp ${MY_CASSIS_BITBUCKET_USER_PUBLIC_KEY_PATH} ${DOCKER_LAMP_BUILDER_CONTAINERS_FOLDER_FULLPATH}/php-apache/www/.ssh/id_rsa.pub