## Installation

Via composer as a library


---

## Usage

Once the project is installed, enter the necissery system enviroment variables.
You find the environment variable in the file namned docker-compose-build/deploy-env.sh .
Then call the script.

```
    containers/scripts/deploy-env.sh 
```


Next step is to call the docker-compose command to set up the environment:

```
    docker-compose up --build -d
```


To list all servers:

```
    docker-compose ps
```


To enter the appache server:

```
    docker-compose exec --user www-data php-apache bash
```


In case of need to remov all docker images:

```
    docker-compose down
    docker system prune -a
```

---

## Copyright and license

Docker lamp script builder as composer library

Copyright (C) 2019  Kjell-Åke Lundblad - Endless-Dreams 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.