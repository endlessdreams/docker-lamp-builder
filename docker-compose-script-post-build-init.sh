#!/bin/bash
# Post build

exec  docker-compose exec -T --user www-data php-apache bash -c "/var/build/init.sh $*"
